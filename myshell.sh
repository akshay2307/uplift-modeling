#!/bin/bash
files=`git diff --name-only ${CI_COMMIT_BEFORE_SHA}...${CI_COMMIT_SHA} `
for file in $files; do
  echo $file
done

echo $username

version_regex = "[0-9]+.[0-9]+.[0-9]+"

function myfunc()
{
    if[[$1 =~ $version_regex]]
    then
      echo $1
    else
      echo "not matched"
    fi
}

myfunc "0.1.0"
